﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment4_SMTP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            MailMessage mail = new MailMessage(textBox1.Text, textBox2.Text, textBox3.Text, richTextBox1.Text);
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.Port = 587;
            client.UseDefaultCredentials = false;

            client.Credentials = new System.Net.NetworkCredential(textBox4.Text, textBox5.Text);
            client.EnableSsl = true;
            client.Send(mail);
            MessageBox.Show("mail sent", "succesfully", MessageBoxButtons.OK);
        }
    }
}
